# Hermios

Réseau social pour android

## Configuration

Installer la librairie volley : https://developer.android.com/training/volley/index.html

Le dossier web à la racine du projet contient toutes les ressources nécessaires pour le côté serveur de l'application : 

+ Modifier le chemin vers le site web dans strings.xml (ligne 88)
+ Modifier dbLogin.php dans web/assets/

Changer la clé pour les APIs de Google dans google-services.json, strings.xml (ligne 94) et web/registerUser.php (ligne 31)

## Description

Tour non exhaustif des fonctionnalités de l'application :

+ Créer un compte via Google (LoginActivity.java)
+ Publier un message avec une image optionnelle, associé à la géolocalisation de l'utilisateur (postCairnActivity.java)
+ Voir toutes les publications du réseau dans un rayon de 600 mètres, valeur arbitraire modifiable dans web/searchCairns.php (ligne 20)
+ Commenter des publications, mettre une publication en favori (CairnActivity.java)
+ Et aussi modifier sa photo de profil, rechercher des publications selon des hashtags...

Le côté serveur a été implémenté from scratch, sans frameworks additionnels. Idem pour le côté client.

## Crédits

Développeur : Santangelo Mike