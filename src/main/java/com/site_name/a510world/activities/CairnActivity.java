package com.site_name.a510world.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.site_name.a510world.R;
import com.site_name.a510world.classes.MySingleton;
import com.site_name.a510world.classes.StaticMethods;
import com.site_name.a510world.fragments.CommentsDialogFragment;

import java.util.HashMap;
import java.util.Map;

public class CairnActivity extends AppCompatActivity implements CommentsDialogFragment.NoticeDialogListener {

    // Upvotes
    String upvoteString;
    private ImageButton star;
    private int upvotes;
    private int resourceId;

    // Cairn
    private TextView nameCairn;
    private TextView dateCairn;
    private TextView messageCairn;
    private TextView numberUpvotes;
    private ImageView iconCairn;
    private ImageView imageCairn;

    // UI
    private View progressBar;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private ScrollView scrollView;

    // Extra data
    private SharedPreferences userDetails;
    private Intent lastIntent;

    // http status code
    private int httpCode = 0;

    // Recycler view for comments
    protected RecyclerView mRecyclerView;
    protected CairnActivity.CustomAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    // Comments view
    protected String[] mMessage;
    protected String[] mName;
    protected String[] mDate;
    protected int[] mIdUsers;
    protected String[] mIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cairn);
        setTitle(getString(R.string.title_cairn));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        lastIntent = getIntent();
        // sub_claim and password
        userDetails = getSharedPreferences(
                getString(R.string.user_details_file_key), Context.MODE_PRIVATE);

        // Get view to show progressBar
        mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        progressBar = findViewById(R.id.progressBar);

        // Cairn views
        dateCairn = (TextView) findViewById(R.id.date_cairn);
        nameCairn = (TextView) findViewById(R.id.name_cairn);
        messageCairn = (TextView) findViewById(R.id.message_cairn);
        numberUpvotes = (TextView) findViewById(R.id.number_upvotes);
        iconCairn = (ImageView) findViewById(R.id.icon_cairn);
        imageCairn = (ImageView) findViewById(R.id.image_cairn);

        // UpVote
        star = (ImageButton) findViewById(R.id.star);

        // Comments UI
        LinearLayout commentSection = (LinearLayout) findViewById(R.id.type_comment_section);

        commentSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialog = CommentsDialogFragment.newInstance();
                dialog.show(getSupportFragmentManager(), "CommentsDialogFragment");
            }
        });

        // To prevent onRefresh if the user is scrolling scroll_view
        scrollView = (ScrollView) findViewById(R.id.scroll_view);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                if (scrollY == 0) mySwipeRefreshLayout.setEnabled(true);
                else mySwipeRefreshLayout.setEnabled(false);

            }
        });

        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
        */
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        // This method performs the actual data-refresh operation.
                        initDataset(true);
                    }
                }
        );

        // Initialize dataset from server
        initDataset(false);

        // BEGIN_INCLUDE(initializeRecyclerView)
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setNestedScrollingEnabled(false);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        // END_INCLUDE(initializeRecyclerView)
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the CommentsDialogFragment.CommentsDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, final String commentText) {
        // User touched the dialog's positive button
        final String urlComment = getString(R.string.site_name) + "commentCairn.php";

        StaticMethods.showProgress(true, mySwipeRefreshLayout, progressBar);

        // upvote server side
        StringRequest attemptComment = new StringRequest(Request.Method.POST, urlComment,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("tag", response);
                        StaticMethods.showProgress(false, mySwipeRefreshLayout, progressBar);
                        // update dataset if comment has been posted
                        if (!TextUtils.isEmpty(response)) {
                            initDataset(false);
                        } else {
                            Snackbar snackbar = Snackbar.make(mySwipeRefreshLayout, R.string.error_invalid_credentials, Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        StaticMethods.showProgress(false, mySwipeRefreshLayout, progressBar);
                        Snackbar snackbar = Snackbar.make(mySwipeRefreshLayout, R.string.error_network, Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                params.put("message", commentText);
                params.put("cairn_id", lastIntent.getStringExtra("cairn_id"));
                return params;
            }
        };

        MySingleton.getInstance(CairnActivity.this).addToRequestQueue(attemptComment);
    }

    /**
     * Values for RecyclerView's adapter.
     */
    private void initDataset(final boolean swiping) {
        if (swiping) {
            mySwipeRefreshLayout.setRefreshing(true);
        } else {
            StaticMethods.showProgress(true, mySwipeRefreshLayout, progressBar);
        }

        // get message, likes, comments and date
        String url = getString(R.string.site_name) + "getCairn.php";
        final String urlUpVote = getString(R.string.site_name) + "upVote.php";

        StringRequest attemptGetCairn = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("CAIRN ACTIVITY", response);
                        if (!TextUtils.isEmpty(response) && httpCode == 200) {
                            String data[] = response.split(">");

                            // Comments data
                            mMessage = new String[data.length - 1];
                            mName = new String[data.length - 1];
                            mDate = new String[data.length - 1];
                            mIdUsers = new int[data.length - 1];
                            mIcon = new String[data.length - 1];

                            for (int i = 0; i < data.length; i++) {
                                final String components[] = data[i].split("<");

                                // decode html entities
                                for (int j = 0; j < components.length; j++) {
                                    components[j] = StaticMethods.decodeHtmlEntities(components[j]);
                                }

                                // If it's the cairn
                                if (i == 0) {
                                    // If user has already upvoted
                                    if (components[3].equals("yes")) {
                                        resourceId = R.drawable.btn_star_big_on;
                                        star.setImageResource(resourceId);
                                    } else {
                                        resourceId = R.drawable.btn_star_big_off;
                                        star.setImageResource(resourceId);
                                    }
                                    upvotes = Integer.parseInt(components[2]);

                                    star.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (resourceId == R.drawable.btn_star_big_off) {
                                                // upvote server side
                                                StringRequest attemptUpVote = new StringRequest(Request.Method.POST, urlUpVote,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                            }
                                                        },
                                                        new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {
                                                            }
                                                        }) {
                                                    @Override
                                                    protected Map<String, String> getParams() {
                                                        Map<String, String> params = new HashMap<String, String>();
                                                        params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                                                        params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                                                        params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                                                        params.put("cairn_id", lastIntent.getStringExtra("cairn_id"));
                                                        params.put("up_vote", "true");
                                                        return params;
                                                    }
                                                };

                                                MySingleton.getInstance(CairnActivity.this).addToRequestQueue(attemptUpVote);

                                                // Upvote client side
                                                resourceId = R.drawable.btn_star_big_on;
                                                star.setImageResource(resourceId);
                                                upvotes = upvotes + 1;

                                                // Check if there are multiple upvotes
                                                if (upvotes > 1) {
                                                    upvoteString = getString(R.string.number_upvotes);
                                                } else {
                                                    upvoteString = getString(R.string.number_upvote);
                                                }

                                                numberUpvotes.setText(String.valueOf(upvotes) + " " + upvoteString);

                                            } else {
                                                // Downvote server side
                                                StringRequest attemptDownVote = new StringRequest(Request.Method.POST, urlUpVote,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                Log.i("TAG", response);
                                                            }
                                                        },
                                                        new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {
                                                            }
                                                        }) {
                                                    @Override
                                                    protected Map<String, String> getParams() {
                                                        Map<String, String> params = new HashMap<String, String>();
                                                        params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                                                        params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                                                        params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                                                        params.put("cairn_id", lastIntent.getStringExtra("cairn_id"));
                                                        return params;
                                                    }
                                                };

                                                // Add a request
                                                MySingleton.getInstance(CairnActivity.this).addToRequestQueue(attemptDownVote);

                                                // Downvote client side
                                                resourceId = R.drawable.btn_star_big_off;
                                                star.setImageResource(resourceId);
                                                upvotes = upvotes - 1;

                                                setNumberUpvotes();
                                            }
                                        }
                                    });

                                    messageCairn.setText(components[0] + " " + lastIntent.getStringExtra("hashtags"));
                                    dateCairn.setText(components[1]);
                                    nameCairn.setText(lastIntent.getStringExtra("name"));
                                    setNumberUpvotes();

                                    //TODO: get image from components[5], then setImageBitmap
                                    final String url = getString(R.string.site_name) + "icons/" + components[5] + ".jpg";
                                    Log.i("URL", url);

                                    // Retrieves an image specified by the URL, displays it in the UI.
                                    ImageRequest getIconCairn = new ImageRequest(url,
                                            new Response.Listener<Bitmap>() {
                                                @Override
                                                public void onResponse(Bitmap bitmap) {
                                                    iconCairn.setImageBitmap(bitmap);
                                                }
                                            }, 0, 0, ImageView.ScaleType.FIT_CENTER, null,
                                            new Response.ErrorListener() {
                                                public void onErrorResponse(VolleyError error) {
                                                }
                                            });

                                    MySingleton.getInstance(CairnActivity.this).addToRequestQueue(getIconCairn);

                                    // Get file from cairn if it exists
                                    if (components.length == 7) {
                                        final String fileUrl = getString(R.string.site_name) + "res/" + components[6];

                                        // Retrieves an image specified by the URL, displays it in the UI.
                                        ImageRequest getFileCairn = new ImageRequest(fileUrl,
                                                new Response.Listener<Bitmap>() {
                                                    @Override
                                                    public void onResponse(Bitmap bitmap) {
                                                        imageCairn.setVisibility(View.VISIBLE);
                                                        imageCairn.setImageBitmap(bitmap);
                                                    }
                                                }, 0, 0, ImageView.ScaleType.FIT_CENTER, null,
                                                new Response.ErrorListener() {
                                                    public void onErrorResponse(VolleyError error) {
                                                    }
                                                });

                                        MySingleton.getInstance(CairnActivity.this).addToRequestQueue(getFileCairn);
                                    }


                                    // If user clicks on the icon cairn or name, he gets redirected to profile view
                                    iconCairn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent toProfile = new Intent(CairnActivity.this, ProfileActivity.class);
                                            toProfile.putExtra("user_id", components[4]);
                                            startActivity(toProfile);
                                        }
                                    });

                                    nameCairn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent toProfile = new Intent(CairnActivity.this, ProfileActivity.class);
                                            toProfile.putExtra("user_id", components[4]);
                                            startActivity(toProfile);
                                        }
                                    });
                                } else {
                                    // If this is a comment

                                    // put id from network response
                                    mIdUsers[i - 1] = Integer.parseInt(components[0]);

                                    // Get comment
                                    mMessage[i - 1] = components[1];
                                    mDate[i - 1] = components[2];
                                    mName[i - 1] = components[3];

                                    // icon name
                                    mIcon[i - 1] = components[4];
                                }
                            }

                            mAdapter = new CairnActivity.CustomAdapter(mMessage, mName, mDate, mIdUsers, mIcon);

                            // Set Custom Adapter as the adapter for RecyclerView.
                            mRecyclerView.setAdapter(mAdapter);

                            if (swiping) {
                                mySwipeRefreshLayout.setRefreshing(false);
                            } else {
                                StaticMethods.showProgress(false, mySwipeRefreshLayout, progressBar);
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //network error
                        if (swiping) {
                            mySwipeRefreshLayout.setRefreshing(false);
                        } else {
                            StaticMethods.showProgress(false, mySwipeRefreshLayout, progressBar);
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                params.put("cairn_id", lastIntent.getStringExtra("cairn_id"));
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                if (response != null) {
                    httpCode = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }

        };

        // Add a request
        MySingleton.getInstance(this).addToRequestQueue(attemptGetCairn);
    }

    public void setNumberUpvotes() {
        // Check if there are multiple upvotes
        if (upvotes > 1) {
            upvoteString = getString(R.string.number_upvotes);
        } else {
            upvoteString = getString(R.string.number_upvote);
        }

        numberUpvotes.setText(String.valueOf(upvotes) + " " + upvoteString);
    }

    public class CustomAdapter extends RecyclerView.Adapter<CairnActivity.CustomAdapter.ViewHolder> {

        private String[] mMessage;
        private String[] mDate;
        private String[] mName;
        private int[] mIdUsers;
        private String[] mIcon;

        // BEGIN_INCLUDE(recyclerViewSampleViewHolder)

        /**
         * Provide a reference to the type of views that you are using (custom ViewHolder)
         */
        class ViewHolder extends RecyclerView.ViewHolder {
            private final CardView cardView;

            ViewHolder(View v) {
                super(v);
                // Define click listener for the ViewHolder's View.
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                cardView = (CardView) v.findViewById(R.id.comment_section);
            }

            CardView getCardView() {
                return cardView;
            }
        }
        // END_INCLUDE(recyclerViewSampleViewHolder)


        CustomAdapter(String[] message, String[] name, String[] date, int idUsers[], String icon[]) {
            mMessage = message;
            mIdUsers = idUsers;
            mName = name;
            mDate = date;
            mIcon = icon;
        }

        // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
        // Create new views (invoked by the layout manager)
        @Override
        public CairnActivity.CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            // Create a new view.
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_item, viewGroup, false);

            return new CairnActivity.CustomAdapter.ViewHolder(v);
        }
        // END_INCLUDE(recyclerViewOnCreateViewHolder)

        // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(CairnActivity.CustomAdapter.ViewHolder viewHolder, final int position) {
            // Get element from your dataset at this position and replace the contents of the view
            // with that element
            CardView currentCard = viewHolder.getCardView();

            LinearLayout ll = (LinearLayout) currentCard.getChildAt(1);

            TextView messageView = (TextView) ll.getChildAt(1); // Message
            TextView nameView = (TextView) ll.getChildAt(0); // Name
            TextView dateView = (TextView) ll.getChildAt(2); // Date

            final ImageView iconView = (ImageView) currentCard.getChildAt(0); // Icon

            messageView.setText(mMessage[position]);
            nameView.setText(mName[position]);
            dateView.setText(mDate[position]);

            //TODO: get icon with mIcon
            String url = getString(R.string.site_name) + "icons/" + mIcon[position] + ".jpg";

            Log.i("URL icons comments", url);

            // Retrieves an image specified by the URL, displays it in the UI.
            ImageRequest getIconComment = new ImageRequest(url,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            iconView.setImageBitmap(bitmap);
                        }
                    }, 0, 0, ImageView.ScaleType.FIT_CENTER, null,
                    new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                        }
                    });

            MySingleton.getInstance(CairnActivity.this).addToRequestQueue(getIconComment);

            // If user clicks on the icon cairn or name, he gets redirected to profile view
            nameView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toProfile = new Intent(CairnActivity.this, ProfileActivity.class);
                    toProfile.putExtra("user_id", mIdUsers[position]);
                    startActivity(toProfile);
                }
            });

            iconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toProfile = new Intent(CairnActivity.this, ProfileActivity.class);
                    toProfile.putExtra("user_id", mIdUsers[position]);
                    startActivity(toProfile);
                }
            });
        }
        // END_INCLUDE(recyclerViewOnBindViewHolder)

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mMessage.length;
        }
    }

    // START MENU

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cairn, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Check if user triggered a refresh:
            case R.id.menu_refresh:
                // Start the refresh background task.
                initDataset(true);
                return true;
        }

        // User didn't trigger a refresh, let the superclass handle this action
        return super.onOptionsItemSelected(item);
    }

    // END MENU

}
