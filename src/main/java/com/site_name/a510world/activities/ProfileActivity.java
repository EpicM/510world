package com.site_name.a510world.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.site_name.a510world.R;
import com.site_name.a510world.classes.MySingleton;
import com.site_name.a510world.classes.StaticMethods;
import com.site_name.a510world.fragments.FavsFragment;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    // Extra data
    private Intent lastIntent;
    private SharedPreferences userDetails;

    // UI
    private CoordinatorLayout coordinatorLayout;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Extra data
        lastIntent = getIntent();
        userDetails = ProfileActivity.this.getSharedPreferences(
                getString(R.string.user_details_file_key), Context.MODE_PRIVATE);

        setContentView(R.layout.activity_profile);

        //TODO: Replace by username
        setTitle(getString(R.string.title_post_cairn));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        // Get UI elements
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        StaticMethods.showProgress(true, coordinatorLayout, progressBar);

        // Get profile informations
        final String urlProfile = getString(R.string.site_name) + "getProfile.php";

        StringRequest getProfile = new StringRequest(Request.Method.POST, urlProfile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        StaticMethods.showProgress(false, coordinatorLayout, progressBar);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //network error
                        StaticMethods.showProgress(false, coordinatorLayout, progressBar);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                params.put("profile_id", lastIntent.getStringExtra("user_id"));
                return params;
            }
        };

        // Add a request
        MySingleton.getInstance(ProfileActivity.this).addToRequestQueue(getProfile);

    }

    // START MENU

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // END MENU
}
