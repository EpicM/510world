package com.site_name.a510world.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.site_name.a510world.R;
import com.site_name.a510world.classes.CustomMarker;
import com.site_name.a510world.classes.MyItem;
import com.site_name.a510world.classes.MySingleton;
import com.site_name.a510world.classes.StaticMethods;

import java.util.HashMap;
import java.util.Map;


public class HashtagSearchActivity extends AppCompatActivity implements OnMapReadyCallback {

    // Extra data
    private SharedPreferences userDetails;
    private String hashtag;
    
    private GoogleMap mMap;
    // Declare a variable for the cluster manager.
    private ClusterManager<MyItem> mClusterManager;

    // UI
    private FrameLayout frameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handleIntent(getIntent());

        setContentView(R.layout.activity_hashtag_search);
        setTitle(getString(R.string.title_hashtag_search));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        // sub_claim, id and password
        userDetails = getSharedPreferences(
                getString(R.string.user_details_file_key), Context.MODE_PRIVATE);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            hashtag = intent.getStringExtra(SearchManager.QUERY);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        setUpClusterer();
    }

    private void setUpClusterer() {
        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<MyItem>(this, mMap);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);

        // Add cluster items (markers) to the cluster manager.
        addItems();
    }

    private void addItems() {
        if (hashtag != null) {
            // Get results from server
            String url = getString(R.string.site_name) + "getHashtags.php";

            StringRequest attemptSearchHashtags = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (!TextUtils.isEmpty(response)) {

                                String data[] = response.split(">");

                                for (String aData : data) {
                                    // FIRST, split the message with the characters protected by htmlspecialchars provided by the server-side
                                    String components[] = aData.split("<");

                                    // THEN decode html entities
                                    for (int j = 0; j < components.length; j++) {
                                        components[j] = StaticMethods.decodeHtmlEntities(components[j]);
                                    }

                                    MyItem offsetItem = new MyItem(Double.parseDouble(components[0]), Double.parseDouble(components[1]));
                                    mClusterManager.addItem(offsetItem);
                                }

                                Snackbar snackbar = Snackbar.make(frameLayout, getString(R.string.zoom_hashtags), Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //network error
                            Snackbar snackbar = Snackbar.make(frameLayout, getString(R.string.error_network), Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                    params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                    params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                    params.put("hashtag", hashtag);
                    return params;
                }
            };

            // Add a request
            MySingleton.getInstance(this).addToRequestQueue(attemptSearchHashtags);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
