package com.site_name.a510world.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.site_name.a510world.classes.MySingleton;
import com.site_name.a510world.R;
import com.site_name.a510world.classes.StaticMethods;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    // UI references
    private View frameLayout;
    private View loginFormView;
    private View progressBar;

    private SharedPreferences userDetails;

    private GoogleApiClient mGoogleApiClient;

    private static final int RC_SIGN_IN = 0;

    private int httpCode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        setContentView(R.layout.activity_login);
        setTitle(getString(R.string.title_login));

        loginFormView = findViewById(R.id.login_form_view);
        progressBar = findViewById(R.id.post_progress);
        frameLayout = findViewById(R.id.frame_layout);

        // if id token and password are in user details, connect directly

        userDetails = LoginActivity.this.getSharedPreferences(
                getString(R.string.user_details_file_key), Context.MODE_PRIVATE);

        if (userDetails.getString(getString(R.string.user_details_sub), null) != null && userDetails.getString(getString(R.string.user_details_password), null) != null && userDetails.getString(getString(R.string.user_details_id), null) != null) {

            StaticMethods.showProgress(true, loginFormView, progressBar);

            String registerUrl = getString(R.string.site_name) + "checkCredentialsLogin.php";

            StringRequest register = new StringRequest(Request.Method.POST, registerUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (!TextUtils.isEmpty(response) && httpCode == 200) {
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                LoginActivity.this.finish();
                            } else {
                                //display error
                                setSigninButton();
                                StaticMethods.showProgress(false, loginFormView, progressBar);
                                Snackbar snackbar = Snackbar.make(frameLayout, getString(R.string.error_sign_in), Snackbar.LENGTH_SHORT);
                                snackbar.show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //network error
                            setSigninButton();
                            StaticMethods.showProgress(false, loginFormView, progressBar);
                            Snackbar snackbar = Snackbar.make(frameLayout, getString(R.string.error_network), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                    params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                    params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                    params.put("login", "true");
                    return params;
                } @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    if (response != null) {
                        httpCode = response.statusCode;
                    }
                    return super.parseNetworkResponse(response);
                }

            };

            // Add a request
            MySingleton.getInstance(this).addToRequestQueue(register);
        } else {
            setSigninButton();
        }
    }

    private void setSigninButton() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customize sign-in button. The sign-in button can be displayed in
        // multiple sizes and color schemes. It can also be contextually
        // rendered based on the requested scopes. For example. a red button may
        // be displayed when Google+ scopes are requested, but a white button
        // may be displayed when only basic profile is requested. Try adding the
        // Scopes.PLUS_LOGIN scope to the GoogleSignInOptions to see the
        // difference.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     */
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            StaticMethods.showProgress(true, loginFormView, progressBar);

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            final String idToken = acct.getIdToken();

            String registerUrl = getString(R.string.site_name) + "registerUser.php";

            StringRequest register = new StringRequest(Request.Method.POST, registerUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (!TextUtils.isEmpty(response) && httpCode == 200) {
                                //store sub claim and password
                                String credentials[] = response.split("\r\n");

                                Log.i("TAG", response);

                                SharedPreferences.Editor editor = userDetails.edit();
                                editor.putString(getString(R.string.user_details_sub), credentials[0]);
                                editor.putString(getString(R.string.user_details_password), credentials[1]);
                                editor.putString(getString(R.string.user_details_id), credentials[2]);
                                editor.apply();

                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                LoginActivity.this.finish();

                            } else {
                                //display error
                                StaticMethods.showProgress(false, loginFormView, progressBar);
                                Snackbar snackbar = Snackbar.make(frameLayout, getString(R.string.error_sign_in), Snackbar.LENGTH_SHORT);
                                snackbar.show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //network error
                            StaticMethods.showProgress(false, loginFormView, progressBar);
                            Snackbar snackbar = Snackbar.make(frameLayout, getString(R.string.error_network), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("idToken", idToken);
                    return params;
                } @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    if (response != null) {
                        httpCode = response.statusCode;
                    }
                    return super.parseNetworkResponse(response);
                }

            };

            // Add a request
            MySingleton.getInstance(this).addToRequestQueue(register);
        } else {
            // Signed out, show unauthenticated UI.
            Snackbar snackbar = Snackbar.make(frameLayout, getString(R.string.error_sign_in), Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
