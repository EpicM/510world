package com.site_name.a510world.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.site_name.a510world.R;
import com.site_name.a510world.activities.CairnActivity;
import com.site_name.a510world.classes.CustomMarker;
import com.site_name.a510world.classes.MySingleton;
import com.site_name.a510world.classes.StaticMethods;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Map Fragment
 */
public class MapFragment extends Fragment implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    // Google API services
    private GoogleApiClient mGoogleApiClient;
    private boolean requestingLocation;
    // Location
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private double distance;

    private UiSettings settingsMap;
    private GoogleMap googleMap;
    private CustomMarker[] customMarkers = new CustomMarker[2000];

    private SharedPreferences userDetails;

    //check
    static final int REQUEST_CHECK_SETTINGS = 1;

    // http request
    private int httpCode = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();

        // START LOCATION CHECK

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        if (mGoogleApiClient.isConnected() && !requestingLocation) {
                            startLocationUpdates();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    getActivity(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });

        // END LOCATION CHECK
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        MapView mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                settingsMap = mMap.getUiSettings();
                settingsMap.setAllGesturesEnabled(false);

                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                    @Override
                    public void onInfoWindowClick(Marker selectedMarker) {
                        Intent toCairnActivity = new Intent(getActivity(), CairnActivity.class);
                        // determine ID CORRESPONDING TO SELECTED MARKER, and add id FROM DATABASE SERVER to the intent
                        for (CustomMarker customMarker : customMarkers) {
                            if (customMarker != null && customMarker.getMarker().getId().equals(selectedMarker.getId())) {
                                toCairnActivity.putExtra("cairn_id", customMarker.getId());
                                Log.i("TAG", customMarker.getId());
                                toCairnActivity.putExtra("hashtags", customMarker.getMarker().getTitle());
                                toCairnActivity.putExtra("name", customMarker.getMarker().getSnippet());
                                startActivity(toCairnActivity);
                            }
                        }
                    }
                });
            }
        });

        return rootView;
    }

    // START LIFECYCLE

    @Override
    public void onStart() {
        super.onStart();
        Log.i("MAP FRAGMENT", "OnStart");
        requestingLocation = false;
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        if (requestingLocation) {
            requestingLocation = false;
        }
    }

    // END LIFECYCLE

    // START GET LOCATION

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("MAP FRAGMENT", "onConnected");

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            updateUI();
        }
        Log.i("MAP FRAGMENT request", String.valueOf(requestingLocation));
        if (!requestingLocation) {
            Log.i("MAP FRAGMENT", "Request location updates");
            startLocationUpdates();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(7500);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        requestingLocation = true;
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("MAP FRAGMENT", "on Location changed");

        if (mLastLocation != null) {
            distance = distance + mLastLocation.distanceTo(location);
            mLastLocation = location;
        } else {
            mLastLocation = location;
            updateUI();
        }
        // If user made 20 meters
        if (distance > 20) {
            distance = 0;
            updateUI();
        }
    }

    // END GET LOCATION

    private void updateUI() {
        Log.i("MAP FRAGMENT", "update UI");
        if (googleMap != null) {
            Log.i("MAP FRAGMENT", "google map is not null");

            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 15));
            settingsMap.setAllGesturesEnabled(true);

            userDetails = getActivity().getSharedPreferences(
                    getString(R.string.user_details_file_key), Context.MODE_PRIVATE);

            String url = getString(R.string.site_name) + "searchCairns.php";

            StringRequest attemptSearchCairns = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (!TextUtils.isEmpty(response) && httpCode == 200) {
                                Log.i("TAG", response);

                                // If there are markers in the map, remove them before update
                                for (CustomMarker customMarker : customMarkers) {
                                    if (customMarker != null) {
                                        customMarker.getMarker().remove();
                                    }
                                }

                                // display cairns
                                String cairns[] = response.split(">");
                                for (int i = 0; i < cairns.length; i++) {
                                    // FIRST, split the message with the characters protected by htmlspecialchars provided by the server-side
                                    String components[] = cairns[i].split("<");

                                    // THEN decode html entities
                                    for (int j = 0; j < components.length; j++) {
                                        components[j] = StaticMethods.decodeHtmlEntities(components[j]);
                                    }

                                    // store id from database server, and put marker onto the map
                                    customMarkers[i] = new CustomMarker((googleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(Double.parseDouble(components[2]), Double.parseDouble(components[3])))
                                            .title(components[1])
                                            .snippet(components[0]))), components[4]);
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //network error

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                    params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                    params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                    params.put("latitude", String.valueOf(mLastLocation.getLatitude()));
                    params.put("longitude", String.valueOf(mLastLocation.getLongitude()));
                    return params;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    if (response != null) {
                        httpCode = response.statusCode;
                    }
                    return super.parseNetworkResponse(response);
                }

            };

            // Add a request
            MySingleton.getInstance(getContext()).addToRequestQueue(attemptSearchCairns);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public MapFragment() {
    }
}