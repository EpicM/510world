package com.site_name.a510world.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.site_name.a510world.R;
import com.site_name.a510world.activities.CairnActivity;
import com.site_name.a510world.activities.LoginActivity;
import com.site_name.a510world.activities.ProfileActivity;
import com.site_name.a510world.classes.MySingleton;
import com.site_name.a510world.classes.StaticMethods;

import java.util.HashMap;
import java.util.Map;

public class FavsFragment extends Fragment {
    private View rootView;

    // UI
    private View progressBar;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private ScrollView scrollView;

    // Recycler view for comments
    protected RecyclerView mRecyclerView;
    protected FavsFragment.CustomAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    // Comments view
    protected String[] mName;
    protected String[] mDate;
    protected int[] mCairnId;
    protected String[] mHashtag;
    protected String[] mIcon;
    protected int[] mIdUsers;

    // Extra data
    private SharedPreferences userDetails;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favs, container, false);

        // Get view to show progressBar
        mySwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        progressBar = rootView.findViewById(R.id.progressBar);

        userDetails = getActivity().getSharedPreferences(
                getString(R.string.user_details_file_key), Context.MODE_PRIVATE);

        // To prevent onRefresh if the user is scrolling scroll_view
        scrollView = (ScrollView) rootView.findViewById(R.id.scroll_view);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                if (scrollY == 0) mySwipeRefreshLayout.setEnabled(true);
                else mySwipeRefreshLayout.setEnabled(false);
            }
        });

        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
        */
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        // This method performs the actual data-refresh operation.
                        initDataset(true);
                    }
                }
        );

        // Initialize dataset from server
        initDataset(false);

        // BEGIN_INCLUDE(initializeRecyclerView)
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setNestedScrollingEnabled(false);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getContext());

        mRecyclerView.setLayoutManager(mLayoutManager);
        // END_INCLUDE(initializeRecyclerView)

        return rootView;
    }

    /**
     * Values for RecyclerView's adapter.
     */
    private void initDataset(final boolean swiping) {
        if (swiping) {
            mySwipeRefreshLayout.setRefreshing(true);
        } else {
            StaticMethods.showProgress(true, mySwipeRefreshLayout, progressBar);
        }

        // get favorites
        String url = getString(R.string.site_name) + "getFavorites.php";

        StringRequest attemptGetFavorites = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("TAG", response);
                        if (!TextUtils.isEmpty(response)) {
                            // Split string response from server
                            String data[] = response.split(">");

                            // Favorites data
                            mName = new String[data.length];
                            mDate = new String[data.length];
                            mCairnId = new int[data.length];
                            mHashtag = new String[data.length];
                            mIcon = new String[data.length];
                            mIdUsers = new int[data.length];

                            for (int i = 0; i < data.length; i++) {
                                String components[] = data[i].split("<");

                                for (int j = 0; j < components.length; j++) {
                                    components[j] = StaticMethods.decodeHtmlEntities(components[j]);
                                }

                                mCairnId[i] = Integer.parseInt(components[0]);
                                mHashtag[i] = components[1];
                                mDate[i] = components[2];
                                mName[i] = components[3];
                                mIdUsers[i] = Integer.parseInt(components[4]);

                                if (components.length == 6)
                                    mIcon[i] = components[5];
                            }

                            mAdapter = new FavsFragment.CustomAdapter(mName, mDate, mCairnId, mHashtag, mIcon, mIdUsers);

                            // Set Custom Adapter as the adapter for RecyclerView.
                            mRecyclerView.setAdapter(mAdapter);
                        }

                        if (swiping) {
                            mySwipeRefreshLayout.setRefreshing(false);
                        } else {
                            StaticMethods.showProgress(false, mySwipeRefreshLayout, progressBar);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //network error
                        if (swiping) {
                            mySwipeRefreshLayout.setRefreshing(false);
                        } else {
                            StaticMethods.showProgress(false, mySwipeRefreshLayout, progressBar);
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sub_claim", userDetails.getString(getString(R.string.user_details_sub), null));
                params.put("password", userDetails.getString(getString(R.string.user_details_password), null));
                params.put("user_id", userDetails.getString(getString(R.string.user_details_id), null));
                return params;
            }
        };

        // Add a request
        MySingleton.getInstance(getContext()).addToRequestQueue(attemptGetFavorites);
    }

    public class CustomAdapter extends RecyclerView.Adapter<FavsFragment.CustomAdapter.ViewHolder> {

        private String[] mDate;
        private String[] mName;
        private String[] mHashtag;
        private int[] mCairnId;
        private String[] mIcon;
        private int[] mIdUsers;


        // BEGIN_INCLUDE(recyclerViewSampleViewHolder)

        /**
         * Provide a reference to the type of views that you are using (custom ViewHolder)
         */
        class ViewHolder extends RecyclerView.ViewHolder {
            private final CardView cardView;

            ViewHolder(View v) {
                super(v);
                // Define click listener for the ViewHolder's View.
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                cardView = (CardView) v.findViewById(R.id.comment_section);

                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // go to CairnActivity
                        Intent toCairn = new Intent(getActivity(), CairnActivity.class);
                        toCairn.putExtra("cairn_id", String.valueOf(mCairnId[getLayoutPosition()]));
                        toCairn.putExtra("name", mName[getLayoutPosition()]);
                        toCairn.putExtra("hashtags", mHashtag[getLayoutPosition()]);

                        startActivity(toCairn);
                    }
                });
            }

            CardView getCardView() {
                return cardView;
            }
        }
        // END_INCLUDE(recyclerViewSampleViewHolder)


        CustomAdapter(String[] name, String[] date, int cairnId[], String hashtag[], String icon[], int idUsers[]) {
            mCairnId = cairnId;
            mName = name;
            mDate = date;
            mHashtag = hashtag;
            mIcon = icon;
            mIdUsers = idUsers;
        }

        // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
        // Create new views (invoked by the layout manager)
        @Override
        public FavsFragment.CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            // Create a new view.
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_item, viewGroup, false);

            return new FavsFragment.CustomAdapter.ViewHolder(v);
        }
        // END_INCLUDE(recyclerViewOnCreateViewHolder)

        // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(FavsFragment.CustomAdapter.ViewHolder viewHolder, final int position) {
            // Get element from your dataset at this position and replace the contents of the view
            // with that element
            CardView currentCard = viewHolder.getCardView();

            LinearLayout ll = (LinearLayout) currentCard.getChildAt(1);

            TextView messageView = (TextView) ll.getChildAt(1); // Hashtags
            TextView nameView = (TextView) ll.getChildAt(0); // Name
            TextView dateView = (TextView) ll.getChildAt(2); // Date

            final ImageView iconView = (ImageView) currentCard.getChildAt(0); // Icon

            messageView.setText(mHashtag[position]);
            nameView.setText(mName[position]);
            dateView.setText(mDate[position]);

            //TODO: get icon with mIcon
            final String url = getString(R.string.site_name) + "icons/" + mIcon[position] + ".jpg";

            // Retrieves an image specified by the URL, displays it in the UI.
            ImageRequest getIconComment = new ImageRequest(url,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            iconView.setImageBitmap(bitmap);
                        }
                    }, 0, 0,ImageView.ScaleType.FIT_CENTER, null,
                    new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                        }
                    });

            // If user clicks on the icon cairn or name, he gets redirected to profile view
            nameView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toProfile = new Intent(getActivity(), ProfileActivity.class);
                    toProfile.putExtra("user_id", mIdUsers[position]);
                    startActivity(toProfile);
                }
            });

            iconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toProfile = new Intent(getActivity(), ProfileActivity.class);
                    toProfile.putExtra("user_id", mIdUsers[position]);
                    startActivity(toProfile);
                }
            });

            MySingleton.getInstance(getActivity()).addToRequestQueue(getIconComment);
        }
        // END_INCLUDE(recyclerViewOnBindViewHolder)

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mHashtag.length;
        }
    }
}
