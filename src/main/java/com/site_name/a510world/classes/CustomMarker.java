package com.site_name.a510world.classes;

import com.google.android.gms.maps.model.Marker;

public class CustomMarker {
    private Marker marker;
    // Id from database server
    private String id;

    public CustomMarker(Marker mMarker, String mId) {
        marker = mMarker;
        id = mId;
    }

    public Marker getMarker () {
        return marker;
    }

    public String getId() {
        return id;
    }
}
