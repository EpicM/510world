<?php
	header('Content-type: text/html; charset=utf-8');
	
	if (isset($_POST["sub_claim"]) && isset($_POST["password"]) && $_POST["user_id"] !== null && isset($_POST["name"]) && strlen($_POST["name"] <= 255)) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			
			$req = $db->
				prepare('UPDATE user SET name = ? WHERE user_id = ?');
			
			$req->bindParam(1, $_POST["name"], PDO::PARAM_STR);
			$req->bindParam(2, $_POST["user_id"], PDO::PARAM_INT);
			
			$req->execute();
		}
	}
?>