<?php
	if (isset($_POST["cairn_id"]) && $_POST["user_id"] !== null && isset($_POST["sub_claim"]) && isset($_POST["password"])) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			$_POST["cairn_id"] = intval($_POST["cairn_id"]);
			
			require("assets/dbLogin.php");
			require_once("assets/getTime.php");
			
			// get cairn
			$req = $db->
				prepare('SELECT * FROM cairn WHERE id = ?');
			$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
			$req->execute();
			
			$idUsers = "";
			
			// If user upvotes
			if (isset($_POST["up_vote"])) {
				while ($data = $req->fetch()) {
					if (boolval($data["up_votes"])) {
						// Keep last ids and put id from user that upvoted
						$idUsers = $data["up_votes"] . $_POST["user_id"] . ";";
					} else {
						$idUsers = $_POST["user_id"] . ";";
					}
				
					$req = $db->
						prepare('UPDATE cairn SET up_votes = ? WHERE id = ?');
					$req->execute(array($idUsers, $_POST["cairn_id"]));
					
					// Check if this fav is already in table
					$req = $db->
						prepare('SELECT user_id, cairn_id FROM favorite WHERE user_id = ? AND cairn_id = ?');
					$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
					$req->bindParam(2, $_POST["cairn_id"], PDO::PARAM_INT);
					$req->execute();
					
					if ($req->fetch() == null) {
						// Put this cairn to favs
						$req = $db->
							prepare('INSERT INTO favorite (user_id, cairn_id) VALUES(?, ?)');
						$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
						$req->bindParam(2, $_POST["cairn_id"], PDO::PARAM_INT);
						$req->execute();
					}
				}
			} else {
				// if user deletes his upvote
				while ($data = $req->fetch()) {
					// Keep last ids and delete id from user that upvoted
					$idUsers = str_replace($_POST["user_id"], "", $data["up_votes"]);
					$idUsers = str_replace(";;", ";", $idUsers);
					
					$idUsers = preg_replace("#(?<!.);#", "", $idUsers);
					
					echo $idUsers;
				
					$req = $db->
						prepare('UPDATE cairn SET up_votes = ? WHERE id = ?');
					$req->execute(array($idUsers, $_POST["cairn_id"]));
				}
			}
			
			$db = null;
			$req = null;
		}
	}
?>