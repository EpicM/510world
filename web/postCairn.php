<?php
	header('Content-type: text/html; charset=utf-8');
	require_once("assets/checkInterval.php");
	
	// Delete spaces at end and debut
	$_POST["message"] = trim($_POST["message"]);
	
	// CHECK if message does not contain antislashes, and if it's not too long or too short
	if (isset($_POST["sub_claim"]) && isset($_POST["password"]) && isset($_POST["message"]) && isset($_POST["latitude"]) && isset($_POST["longitude"]) && is_numeric($_POST["latitude"]) && is_numeric($_POST["longitude"]) && within($_POST["longitude"], -180, 180) && within($_POST["latitude"], -90, 90) && within(strlen($_POST["message"]), 4, 2000) && $_POST["user_id"] !== null) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			
			$datediff = false;
			
			// CHECK IF NOW() - LAST DROP > 1 minute
			$req = $db->
				prepare('SELECT * FROM user WHERE user_id = ? AND TIMESTAMPDIFF(MINUTE, last_drop, NOW()) > 0');
			$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
			$req->execute();
			
			while ($data = $req->fetch()) {
				$datediff = true;
			}
			
			if ($datediff) {
				// Get hashtags
				$count = preg_match_all("#\#(\w+)#", $_POST["message"], $matches);

				$hashtags = "";
	
				for ($i = 0; $i < $count; $i++) {
					if ($i == 0) {
						$hashtags = $matches[0][$i];
						$_POST["message"] = str_replace($matches[0][$i] , "", $_POST["message"]);
					} else {
						$hashtags = $hashtags . " " . $matches[0][$i];
						$_POST["message"] = str_replace($matches[0][$i] , "", $_POST["message"]);
					}
				}
				
				$fileName = "";
				
				if (isset($_FILES["image"])) {
					// upload file
					$fileName = md5(uniqid(rand(), true)) . ".jpg";
		
					$fileExtension = substr(strrchr($_FILES["image"]["name"], '.'), 1);
					$image_sizes = getimagesize($_FILES["image"]["tmp_name"]);
					
					$maxwidth = 1008;
					$maxheight = 1344;
					$maxsize = 2100000;
		
					$extensions = array("jpg", "jpeg", "png");
		
					if (in_array($fileExtension, $extensions) && $_FILES["image"]["size"] <= $maxsize && $image_sizes[0] <= $maxwidth && $image_sizes[1] <= $maxheight)
						$upload = move_uploaded_file($_FILES["image"]["tmp_name"], "res/" . $_POST["user_id"] . "/" . $fileName);
				}
				
				// UPDATE LAST DROP DATE
				$req = $db->
					prepare('UPDATE user SET last_drop = NOW() WHERE user_id = ?');
				$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
				$req->execute();
	
				$req = $db->
					prepare('INSERT INTO cairn (user_id, message, hashtag, file_name, lng_lat, date) VALUES (?, ?, ?, ?, PointFromText("POINT(' . $_POST["longitude"] . ' ' . $_POST["latitude"] . ')"), NOW())');
				$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
				$req->bindParam(2, $_POST["message"], PDO::PARAM_STR);
				$req->bindParam(3, $hashtags, PDO::PARAM_STR);
				$req->bindParam(4, $fileName, PDO::PARAM_STR);
				
				$req->execute();
				
				echo "cairn sent";
			}
			
			$db = null;
			$req =  null;
		}
	}
?>