<?php
	header('Content-type: text/html; charset=utf-8');
	
	if (isset($_POST["sub_claim"]) && isset($_POST["password"]) && $_POST["user_id"] !== null && isset($_POST["hashtag"])) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			
			// get hashtags
			$req = $db->
				prepare('SELECT *, ST_AsText(lng_lat) FROM cairn WHERE MATCH (hashtag) AGAINST (? IN BOOLEAN MODE)');
			
			$req->execute(array($_POST["hashtag"]));
			
			while ($data = $req->fetch()) {
				// Get latitude and longitude
				preg_match_all("#\d+\.\d+|\d+#", $data["ST_AsText(lng_lat)"], $matches);
				
				// [0][1] --> latitude
				echo $matches[0][1] . "<" . $matches[0][0] . ">";
			}
		}
	}
?>