<?php
	header('Content-type: text/html; charset=utf-8');
	
	if (isset($_POST["sub_claim"]) && isset($_POST["password"]) && $_POST["user_id"] !== null) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			require_once("assets/getTime.php");
			
			// get favorites
			$req = $db->
				prepare('SELECT *, TIMESTAMPDIFF(MINUTE, cairn.date, NOW()) AS datediff, user.user_id AS id_user FROM favorite 
				LEFT JOIN cairn ON favorite.cairn_id = cairn.id
				LEFT JOIN user ON cairn.user_id = user.user_id 
				WHERE favorite.user_id = ? ORDER BY favorite.id DESC');
			
			$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
			$req->execute();
			
			while ($data = $req->fetch()) {
				$data["datediff"] = intval($data["datediff"]);
				if (empty($data["name"])) { $data["name"] = "Anonymous"; }

				echo $data["cairn_id"] . "<" . htmlspecialchars($data["hashtag"]) . "<" . getTime($data["datediff"]) . "<" . $data["name"] . "<" . $data["id_user"];
				
				if ($data["icon_name"] != null)
					echo "<" . $data["id_user"] . "/" . $data["icon_name"];
				
				echo ">";
			}
		}
	}
?>