<?php
	header('Content-type: text/html; charset=utf-8');
	require_once("assets/checkInterval.php");
	
	if (isset($_POST["cairn_id"]) && isset($_POST["message"]) && within(strlen($_POST["message"]), 4, 2000) && isset($_POST["sub_claim"]) && isset($_POST["password"]) && $_POST["user_id"] !== null) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			
			// Check if user already commented this cairn
			$req = $db->
				prepare('SELECT * FROM comment WHERE user_id = ? AND cairn_id = ?');
			$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
			$req->bindParam(2, $_POST["cairn_id"], PDO::PARAM_INT);
			$req->execute();
			
			if ($req->fetch() == null) {
				$req = $db->
					prepare('INSERT INTO comment (user_id, message, cairn_id, date) VALUES (?, ?, ?, NOW())');
				$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
				$req->bindParam(2, $_POST["message"], PDO::PARAM_STR);
				$req->bindParam(3, $_POST["cairn_id"], PDO::PARAM_INT);
				$req->execute();
			}
			
			// Check if this fav is already in table
			$req = $db->
				prepare('SELECT user_id, cairn_id FROM favorite WHERE user_id = ? AND cairn_id = ?');
			$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
			$req->bindParam(2, $_POST["cairn_id"], PDO::PARAM_INT);
			$req->execute();
			
			if ($req->fetch() == null) {
				// Put this cairn to favs
				$req = $db->
					prepare('INSERT INTO favorite (user_id, cairn_id) VALUES(?, ?)');
				$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
				$req->bindParam(2, $_POST["cairn_id"], PDO::PARAM_INT);
				$req->execute();
			}
			
			// Check if there are more than 9 comments on the cairn
			$req = $db->
				prepare('SELECT * FROM comment WHERE cairn_id = ?');
			$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
			$req->execute();
			
			if ($req->rowCount() >= 10) {
				
				// Get image file to delete it
				$req = $db->
					prepare('SELECT * FROM cairn WHERE id = ?');
				$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
				$req->execute();
				
				while ($data = $req->fetch()) {
					if ($data["file_name"] != null) {
						unlink("res/" . $data["user_id"] . "/" . $data["file_name"]);
					}
				}
				
				$req = $db->
					prepare('DELETE FROM comment WHERE cairn_id = ?');
				$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
				$req->execute();
				
				$req = $db->
					prepare('DELETE FROM cairn WHERE id = ?');
				$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
				$req->execute();
				
				$req = $db->
					prepare('DELETE FROM cairn WHERE cairn_id = ?');
				$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
				$req->execute();
			}
			
			echo "comment sent";
		}
	}
?>