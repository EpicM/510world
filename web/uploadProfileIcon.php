<?php
	header('Content-type: text/html; charset=utf-8');
	
	if (isset($_POST["sub_claim"]) && isset($_POST["password"]) && $_POST["user_id"] !== null && isset($_FILES["image"])) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			
			// Retrieve last profile icon name if it exists
			$req = $db->
					prepare('SELECT * FROM user WHERE user_id = ?');
				
			$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
			
			$req->execute();
			
			while ($data = $req->fetch()) {
				// Delete picture
				if ($data["icon_name"] != null)
					unlink("icons/" . $_POST["user_id"] . "/" . $data["icon_name"] . ".jpg");
			}
			
			$fileName = md5(uniqid(rand(), true));
		
			$fileExtension = substr(strrchr($_FILES["image"]['name'], '.'), 1);
			$image_sizes = getimagesize($_FILES['image']['tmp_name']);
		
			$maxwidth = 300;
			$maxheight = 300;
			$maxsize = 100000;
		
			$extensions = array("jpg", "jpeg", "png");
		
			if (in_array($fileExtension, $extensions) && $_FILES["image"]["size"] <= $maxsize && $image_sizes[0] <= $maxwidth && $image_sizes[1] <= $maxheight)
				$upload = move_uploaded_file($_FILES["image"]["tmp_name"], "icons/" . $_POST["user_id"] . "/" . $fileName . ".jpg");
		
			if ($upload) {
				echo $fileName;
				
				$req = $db->
					prepare('UPDATE user SET icon_name = ? WHERE user_id = ?');
				
				$req->bindParam(1, $fileName, PDO::PARAM_STR);
				$req->bindParam(2, $_POST["user_id"], PDO::PARAM_INT);
		
				$req->execute();
			}
		}
	}
?>