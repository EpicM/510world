<?php
	header('Content-type: text/html; charset=utf-8');
	
	if (isset($_POST["cairn_id"]) && isset($_POST["sub_claim"]) && isset($_POST["password"]) && $_POST["user_id"] !== null) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require_once("assets/getTime.php");
			
			$_POST["cairn_id"] = intval($_POST["cairn_id"]);
			
			require("assets/dbLogin.php");
			
			// get cairn
			$req = $db->
				prepare('SELECT *, TIMESTAMPDIFF(MINUTE, date, NOW()) AS datediff FROM cairn LEFT JOIN user ON cairn.user_id = user.user_id WHERE id = ?');
			$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
			$req->execute();
			
			while ($data = $req->fetch()) {
				$upVotes = substr_count($data["up_votes"], ";");
				// Search id from user
				$myUpVote = strpos($data["up_votes"], $_POST["user_id"]);
				
				$data["datediff"] = intval($data["datediff"]);
				
				if ($myUpVote !== false) $str = "yes";
				else $str = "no";
				
				echo htmlspecialchars($data["message"]) . "<" . getTime($data["datediff"]) . "<" . $upVotes . "<" . $str . "<" . $data["user_id"];
				
				if ($data["icon_name"] != null)
					echo "<" . $data["user_id"] . "/" . $data["icon_name"];
				else
					echo "<samples/avatar";
				
				if ($data["file_name"] != null)
					echo "<" . $data["user_id"] . "/" . $data["file_name"];
			}
			
			// get comments
			$req = $db->
				prepare('SELECT *, TIMESTAMPDIFF(MINUTE, date, NOW()) AS datediff FROM comment LEFT JOIN user ON comment.user_id = user.user_id WHERE cairn_id = ? ORDER BY date DESC');
			$req->bindParam(1, $_POST["cairn_id"], PDO::PARAM_INT);
			$req->execute();
			
			while ($data = $req->fetch()) {
				$data["datediff"] = intval($data["datediff"]);
				if (empty($data["name"])) { $data["name"] = "Anonymous"; }
				
				echo ">" . $data["user_id"] . "<" . htmlspecialchars($data["message"]) . "<" . getTime($data["datediff"]) . "<" . $data["name"];
				
				if ($data["icon_name"] != null)
					echo "<" . $data["user_id"] . "/" . $data["icon_name"];
				else
					echo "<samples/avatar";
			}
			
			$db = null;
			$req = null;
		}
	}
?>