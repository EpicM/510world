<?php
function getTime($datediff) {
	$str = "";

	if ($datediff > 60 * 24 * 30 * 12) { // YEAR
		$datediff = intval($datediff / (60 * 24 * 30 * 12));
		$str = " year";

	} else if ($datediff > 60 * 24 * 30) { // MONTH
		$datediff = intval($datediff / (60 * 24 * 30));
		$str = " month";

	} else if ($datediff > 60 * 24 * 7) { // WEEK
		$datediff = intval($datediff / (60 * 24 * 7));
		$str = " week";

	} else if ($datediff > 60 * 24) { // DAY
		$datediff = intval($datediff / (60 * 24));
		$str = " day";

	} else if ($datediff > 60) { // HOUR
		$datediff = intval($datediff / (60));
		$str = " hour";

	} else { // MINUTE
		$str = " minute";
	}

	if ($datediff != 1) $datediff = strval($datediff) . $str . "s"; else $datediff = strval($datediff) . $str;

	return $datediff;
}
?>