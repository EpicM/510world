<?php
function upload($index, $destination, $maxsize = false, $extensions = false) {
	
	// taille limite
    if ($maxsize !== false && $_FILES[$index]['size'] > $maxsize) return false;
	
	// extension
    $ext = substr(strrchr($_FILES[$index]['name'], '.'), 1);
    if ($extensions !== false && !in_array($ext, $extensions)) return false;
	
	// déplacement
    return move_uploaded_file($_FILES[$index]['tmp_name'], $destination);
}
?>
