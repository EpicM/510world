<?php
	if (isset ($_POST['idToken'])) {
		
		$accountExists = false;
		$url = 'https://www.googleapis.com/oauth2/v3/tokeninfo';
			
		// create curl resource 
		$ch = curl_init($url); 
			
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "id_token=" . $_POST["idToken"]);

		//return the transfer as a string 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT,10);

		// $output contains the output string 
		$output = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		// close curl resource to free up system resources 
		curl_close($ch);
			
		if ($httpCode == 200) {
				
			$output = strstr($output, "{");
				
			//Check if aud claim contains one of app's client IDs
			$response = json_decode($output, true);

			if ($response["aud"] == "149538161983-i3l29ckbq31k9agsr62i9bfh7dba58ig.apps.googleusercontent.com" && isset($response["sub"])) {
				$name = "";
				
				if (isset($response["name"])) {
					$name = $response["name"];
				}
				
				require_once("assets/dbLogin.php");
					
				// Check if sub claim is already registered
				$req = $db->
					prepare('SELECT * FROM user WHERE sub_claim IN(?)');
		
				$req->execute(array($response["sub"]));
					
				while ($data = $req->fetch()) {
					$accountExists = true;
					echo $data["sub_claim"] . "\r\n" . $data["password"] . "\r\n" . $data["user_id"];
					
				} if (!$accountExists) {
					$password = password_hash(openssl_random_pseudo_bytes(10, $cstrong), PASSWORD_DEFAULT);
				
					$req = $db->
						prepare('INSERT INTO user (sub_claim, password, name, last_drop, last_request) VALUES (?, ?, ?, NOW(), NOW())');
		
					$req->execute(array($response["sub"], $password, $name));
						
					echo $response["sub"] . "\r\n" . $password;
					
					$req = $db->
						prepare('SELECT * FROM user WHERE sub_claim = ? AND password = ?');
		
					$req->execute(array($response["sub"], $password));
					
					while ($data = $req->fetch()) {
						echo "\r\n" . $data["user_id"];
						// Create directory for icons
						mkdir("icons/" . $data["user_id"], 0777, true);
						fopen("icons/" . $data["user_id"] . "/index.html", "w");
						mkdir("res/" . $data["user_id"], 0777, true);
						fopen("res/" . $data["user_id"] . "/index.html", "w");
					}
				}
			}
		}
		
		$db = null;
		$req = null;
	}
?>