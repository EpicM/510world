<?php
	if (isset ($_POST["sub_claim"]) && isset ($_POST["password"]) && $_POST["user_id"] !== null) {
		require("assets/dbLogin.php");
	
		$req = $db
			->prepare('SELECT * FROM user WHERE user_id IN(?) AND sub_claim IN(?) AND password IN(?)');
		
		$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
		$req->bindParam(2, $_POST["sub_claim"], PDO::PARAM_STR);
		$req->bindParam(3, $_POST["password"], PDO::PARAM_STR);
		$req->execute();
		
		$log = false;
		
		while ($data = $req->fetch()) {
			if (isset($_POST["login"])) {
				echo "Logged in";
			} else {
				$log = true;
			}
		}
		
		$db = null;
		$req = null;
	}
?>