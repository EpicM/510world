<?php
	header('Content-type: text/html; charset=utf-8');
	require_once("assets/checkInterval.php");
	
	if ($_POST["user_id"] !== null && is_numeric($_POST["latitude"]) && is_numeric($_POST["longitude"]) && isset($_POST["latitude"]) && isset($_POST["longitude"]) && within($_POST["longitude"], -180, 180) && within($_POST["latitude"], -90, 90) && isset($_POST["sub_claim"]) && isset($_POST["password"])) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			
			$b = array($_POST["longitude"] - 0.05, $_POST["latitude"] - 0.05);
			$c = array($_POST["longitude"] - 0.05, $_POST["latitude"] + 0.05);
			$d = array($_POST["longitude"] + 0.05, $_POST["latitude"] + 0.05);
			$e = array($_POST["longitude"] + 0.05, $_POST["latitude"] - 0.05);
			
			// retrieve name from the author and get cairns
			$req = $db->
				prepare("SELECT *, ST_AsText(lng_lat) FROM cairn LEFT JOIN user ON cairn.user_id = user.user_id 
				WHERE MBRContains(PolygonFromText('Polygon((" . $b[0] . " " . $b[1] . "," . $c[0] . " " . $c[1] . "," . $d[0] . " " . $d[1] . "," . $e[0] . " " . $e[1] . "," . $b[0] . " " . $b[1] . "))'), lng_lat)
				AND (ACOS(SIN(RADIANS(?)) * SIN(RADIANS(Y(lng_lat))) + COS(RADIANS(?)) * COS(RADIANS(Y(lng_lat))) * COS(RADIANS(X(lng_lat)) - RADIANS(?)))) * 6378137 < 600
				ORDER BY date DESC 
				LIMIT 200");
			
			// Distance
			$req->bindParam(1, $_POST["latitude"], PDO::PARAM_INT);
			$req->bindParam(2, $_POST["latitude"], PDO::PARAM_INT);
			$req->bindParam(3, $_POST["longitude"], PDO::PARAM_INT);
			
			$req->execute();
			
			while ($data = $req->fetch()) {
				if (empty($data["name"])) { $data["name"] = "Anonymous"; }
				
				echo htmlspecialchars($data["name"]);
				
				if ($data["hashtag"] == null) {
					$data["hashtag"] = "No hashtags";
				}
				
				// Get latitude and longitude
				preg_match_all("#\d+\.\d+|\d+#", $data["ST_AsText(lng_lat)"], $matches);
				
				echo "<" . htmlspecialchars($data["hashtag"]) . "<" . $matches[0][1] . "<" . $matches[0][0] . "<" . $data["id"] . ">";
			}
			
			// set last position from user
			$req = $db->
				prepare('UPDATE user SET last_position = PointFromText("POINT(' . $_POST["longitude"] . ' ' . $_POST["latitude"] . ')") WHERE user_id = ?');
			
			$req->bindParam(1, $_POST["user_id"], PDO::PARAM_INT);
			$req->execute();
			
			//TODO: update last request
		}
	}
?>