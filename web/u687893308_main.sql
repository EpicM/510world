
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 23 Décembre 2016 à 19:22
-- Version du serveur: 10.0.20-MariaDB
-- Version de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `u687893308_main`
--

-- --------------------------------------------------------

--
-- Structure de la table `cairn`
--

CREATE TABLE IF NOT EXISTS `cairn` (
  `message` text NOT NULL,
  `hashtag` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `up_votes` text NOT NULL,
  `lng_lat` geometry NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  SPATIAL KEY `lng_lat` (`lng_lat`),
  FULLTEXT KEY `hashtag` (`hashtag`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=94 ;

--
-- Contenu de la table `cairn`
--

INSERT INTO `cairn` (`message`, `hashtag`, `user_id`, `date`, `id`, `up_votes`, `lng_lat`, `file_name`) VALUES
('Super journée', '#disney #ratatouille #spaceMountain', 12, '2016-10-21 16:56:57', 37, '12;', '\0\0\0\0\0\0\0]�u_D@��KoH@', ''),
('mairie   ', '#mairie #choisy #parc', 12, '2016-10-27 13:40:30', 53, '12;', '\0\0\0\0\0\0\0Ojy/F@��\r�aH@', ''),
('petite ballade  ', '#quais #seine', 12, '2016-10-27 13:05:32', 52, '12;17;', '\0\0\0\0\0\0\0\r_^8V@�F���aH@', ''),
('La maison du développeur ', '#android', 12, '2016-12-10 16:03:23', 79, '12;', '\0\0\0\0\0\0\0�D4=[@�F''�fbH@', ''),
('Lycée Guillaume Apollinaire   ', '#thibo #thibz #rambo', 12, '2016-11-07 08:15:56', 57, '12;', '\0\0\0\0\0\0\0�p��4@�� ��`H@', ''),
('test ', '#ios', 12, '2016-12-13 12:50:25', 85, '12;', '\0\0\0\0\0\0\0�:�;a3@�jk��`H@', ''),
('Encore un test ', '#lel', 12, '2016-12-23 18:33:08', 93, '12;', '\0\0\0\0\0\0\0���\0D[@�ЎebH@', '23ed0ef16fff9cf484362847f10f90ca.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `cairn_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cairn_id` (`cairn_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Contenu de la table `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `message`, `cairn_id`, `date`) VALUES
(49, 12, 'jolie chambre', 93, '2016-12-23 19:05:47'),
(48, 17, 'juste un petit test', 52, '2016-12-23 17:29:25'),
(47, 17, 'jsiwbsi', 91, '2016-12-23 15:51:10'),
(46, 12, 'yayyy', 85, '2016-12-22 11:34:55'),
(45, 12, '', 57, '2016-12-14 21:10:34'),
(44, 12, 'Test ', 79, '2016-12-13 21:55:33'),
(43, 12, 'J''aime macéo', 85, '2016-12-13 13:13:43'),
(42, 12, 'bgģgg ! j arrve bouge pas', 57, '2016-12-12 08:14:41'),
(37, 12, '\\xF0\\x9F\\x98\\x81', 52, '2016-12-10 16:11:44'),
(36, 12, 'Oui, effectivement ', 52, '2016-12-10 16:00:28');

-- --------------------------------------------------------

--
-- Structure de la table `favorite`
--

CREATE TABLE IF NOT EXISTS `favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cairn_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`cairn_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `favorite`
--

INSERT INTO `favorite` (`id`, `user_id`, `cairn_id`) VALUES
(5, 12, 79),
(3, 12, 52),
(6, 12, 57),
(7, 12, 85),
(8, 17, 91),
(9, 17, 52),
(10, 12, 93);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_claim` varchar(255) NOT NULL,
  `last_drop` datetime NOT NULL,
  `last_request` datetime NOT NULL,
  `icon_name` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`email`, `name`, `password`, `user_id`, `sub_claim`, `last_drop`, `last_request`, `icon_name`) VALUES
('', 'Mike', '$2y$10$jK3vlHR6JKMfSJD8VHaaB.Tw.9T5PU9phXhI9sDmgc11Z9ji1UmEy', 12, '114746098255167041475', '2016-12-23 18:33:08', '0000-00-00 00:00:00', '6cbd997038de6cdb26797b7313acc601'),
('', 'Epic M', '$2y$10$froNInp1t875NsdN40cxW.EO4VXWtaLgIHHyBaCaNaJdRrDIXKFFi', 17, '100528657844760226899', '2016-12-23 15:50:58', '2016-12-22 18:40:49', 'ca626c4b735a1d55ecaf9f92ca73455b');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
