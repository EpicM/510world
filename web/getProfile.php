<?php
	header('Content-type: text/html; charset=utf-8');
	
	if (isset($_POST["sub_claim"]) && isset($_POST["password"]) && $_POST["user_id"] !== null && isset($_POST["profile_id"])) {
		require_once("checkCredentialsLogin.php");
		
		if ($log) {
			require("assets/dbLogin.php");
			
			// get user profile
			$req = $db->
				prepare('SELECT * FROM user WHERE user_id = ?');
			
			$req->bindParam(1, $_POST["profile_id"], PDO::PARAM_INT);
			$req->execute();
			
			while ($data = $req->fetch()) {
				if (empty($data["name"])) $data["name"] = "Anonymous";
				
				echo $data["name"];
				
				if (!empty($data["icon_name"]))
					echo "<" . $data["icon_name"];
				if (!empty($data["phone_number"]))
					echo ">" . $data["phone_number"];
				if (!empty($data["last_position"]))
					echo "'" . $data["last_position"];
			}
		}
	}
?>